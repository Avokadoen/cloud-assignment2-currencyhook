// Author: Aksel Hjerpbakk
// Studentnr: 997816

package main

import (
	"cloud-assignment2-currencyhook/functionality"
	"net/http"
	"os"
)



func main() {
	functionality.MongoDB = functionality.GetADB()
	functionality.MongoDB.Init()
	port := os.Getenv("PORT")
	http.HandleFunc("/", functionality.URLHandler)
	http.ListenAndServe(":"+port, nil)
}
