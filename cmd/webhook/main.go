package main

import (
	"cloud-assignment2-currencyhook/functionality"
	"time"
)

func main() {

	oneOffSens := 1

	hour, minute, _ := time.Now().UTC().Clock()

	if hour == 17 || hour == 16 && minute <= oneOffSens || minute >= 60 - oneOffSens {
		functionality.MongoDB = functionality.GetADB()
		functionality.MongoDB.Init()
		functionality.MongoDB.DailyCheckHooks()
		functionality.DailyRoutine()
	}
}
