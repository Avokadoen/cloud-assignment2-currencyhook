package functionality

import "gopkg.in/mgo.v2/bson"

type CurrencyLoad struct {
	Id              bson.ObjectId `bson:"_id,omitempty"`
	WebHookURL      string        `json:"webhookURL"`
	BaseCurrency    string        `json:"baseCurrency"`
	TargetCurrency  string        `json:"targetCurrency"`
	MinTriggerValue float64       `json:"minTriggerValue"`
	MaxTriggerValue float64       `json:"maxTriggerValue"`
}

type HookLoad struct {
	BaseCurrency    string  `json:"baseCurrency"`
	TargetCurrency  string  `json:"targetCurrency"`
	CurrentRate     float64 `json:"currentRate"`
	MinTriggerValue float64 `json:"minTriggerValue"`
	MaxTriggerValue float64 `json:"maxTriggerValue"`
}

type RawFixer struct {
	Base      string             `json:"base"`
	Date      string             `json:"date"`
	LocalRate map[string]float64 `json:"rates"`
}

type Rates struct {
	BaseCurrency   string `json:"baseCurrency"`
	TargetCurrency string `json:"targetCurrency"`
}

/*
{
"webhookURL":"https://discordapp.com/api/webhooks/375013560788451331/4NO43EJrdT0pg-BL_RgPXgOuHYpiBvxFMljSxY6BwJapDhzO0wsADvPkcpEarD-LP5kB",
"baseCurrency":"EUR",
"targetCurrency":"NOK",
"minTriggerValue":2.3,
"maxTriggerValue":5.6
}
*/
