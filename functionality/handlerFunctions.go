package functionality

// TODO: replace prints with error handling
// TODO: comment all

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"strings"
)

//RootHandler ...
// Allow user to insert new webhooks into db. Will give user an id in raw body if success
func RootHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {

		r.Header.Set("Content-Type", "application/json")

		newCurrencyLoad, err := getCurrencyLoad(r)
		if err != nil {
			http.Error(w, "Could not retrieve post data in form currencyLoad", http.StatusInternalServerError)
			return
		}

		if newCurrencyLoad.BaseCurrency != "EUR" {
			http.Error(w, "Use EUR as base", http.StatusNotImplemented)
			return
		}
		validString, err := regexp.MatchString("^[A-Z]{3}$", newCurrencyLoad.TargetCurrency)
		if err != nil {
			http.Error(w, "Something went wrong under regexp match", http.StatusInternalServerError)
			return
		} else if validString == false {
			http.Error(w, "Invalid target currency!", http.StatusConflict)
			return
		}

		// TODO: Check with local fixer if target is valid

		err = MongoDB.Add(newCurrencyLoad)
		if err != nil {
			http.Error(w, "Failed to add", http.StatusInternalServerError)
			return
		}

		addedHook, found := MongoDB.Get(newCurrencyLoad.WebHookURL)
		if found == false {
			http.Error(w, "Could not find ID!", http.StatusNotFound)
			return
		}

		frontID := strings.Split(addedHook.Id.String(), "\"")

		fmt.Fprintf(w, "ID: %s", frontID[1])

	} else {
		http.Error(w, "Please post json to create webhook", http.StatusBadRequest)
	}
}

func IdHandler(w http.ResponseWriter, r *http.Request) {

	hookURL := r.URL.Path

	idString := strings.Split(hookURL, "/")

	if r.Method == "GET" {
		webHook, gotIt := MongoDB.GetByID(idString[1])
		if gotIt == false {
			http.Error(w, "Could not find web hook", http.StatusNotFound)
			return
		}
		w.Header().Set("content-type", "application/json")
		json.Marshal(&webHook)
		json.NewEncoder(w).Encode(webHook)

	} else if r.Method == "DELETE" {
		_, got := MongoDB.Get(idString[1])
		if got == true {

			wasDeleted := MongoDB.Delete(idString[1])
			if wasDeleted == false {
				http.Error(w, "Could not find web hook", http.StatusNotFound)
				return
			}
			w.WriteHeader(http.StatusOK)
		} else {
			w.WriteHeader(http.StatusNotFound)
		}
	} else {
		w.WriteHeader(http.StatusConflict)
	}
}

func LatestHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		postRates := Rates{}

		err := json.NewDecoder(r.Body).Decode(&postRates)
		if err != nil {
			http.Error(w, "Could not retrieve data from post", http.StatusConflict)
			return
		}

		rawFixer := RawFixer{}

		DayRate, gotLocal := MongoDB.GetLocalFixer(0)
		if gotLocal == false {
			http.Error(w, "Missing data about today", http.StatusInternalServerError)
			return
		}

		dayRateBody, err := json.Marshal(DayRate)
		if err != nil {
			http.Error(w, "Failed to convert internal data 1", http.StatusInternalServerError)
			return
		}
		err = json.Unmarshal(dayRateBody, &rawFixer)
		if err != nil {
			http.Error(w, "Failed to convert internal data 2", http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
		fmt.Fprintln(w, rawFixer.LocalRate[postRates.TargetCurrency])

	} else {
		http.Error(w, "Wrong method incoming", http.StatusConflict)
		return
	}
}

func AverageHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {

		const AverageOf = 3

		postRates := Rates{}

		err := json.NewDecoder(r.Body).Decode(&postRates)
		if err != nil {
			http.Error(w, "Could not retrieve data from post", http.StatusConflict)
		}

		rawFixer := RawFixer{}

		var rateSum float64

		if MongoDB.CountFix() >= AverageOf {
			for i := 0; i < AverageOf; i++ {

				DayRate, gotLocal := MongoDB.GetLocalFixer(i)
				if gotLocal == false {
					http.Error(w, "Missing data about one of the days", http.StatusInternalServerError)
					return
				}

				dayRateBody, err := json.Marshal(DayRate)
				if err != nil {
					http.Error(w, "Failed to convert internal data", http.StatusInternalServerError)
					return
				}
				err = json.Unmarshal(dayRateBody, &rawFixer)
				if err != nil {
					http.Error(w, "Failed to convert internal data", http.StatusInternalServerError)
					return
				}
				rateSum += rawFixer.LocalRate[postRates.TargetCurrency]
			}

			averageRate := rateSum / AverageOf
			w.WriteHeader(http.StatusOK)
			fmt.Fprintln(w, averageRate)
		}else{
			http.Error(w, "Missing required data", http.StatusInternalServerError)
			return
		}
	} else {
		http.Error(w, "Wrong method incoming", http.StatusConflict)
		return
	}

}

func EvalHandler(w http.ResponseWriter, r *http.Request) {
	MongoDB.EvalDailyCheckHooks()
}

func URLHandler(w http.ResponseWriter, r *http.Request) {
	URLCheck := r.URL.Path
	URLSplit := strings.Split(URLCheck, "/")

	checkString := strings.ToLower(URLSplit[1])

	// TODO: Change to switch?

	if checkString == "" {
		RootHandler(w, r)
		return
	} else if checkString == "latest" {
		LatestHandler(w, r)
	} else if checkString == "average" {
		AverageHandler(w, r)
	} else if checkString == "evaluationtrigger" {
		EvalHandler(w, r)
	} else if strings.Count(checkString, "") == 25 {
		IdHandler(w, r)
	} else{
		http.NotFound(w, r)
	}

}
