package functionality

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

func DailyRoutine() {
	db := GetADB()
	db.Init()
	db.AddDailyFix()
	db.DailyCheckHooks()
}

type WebhookInfo struct {
	Username string `json:"username"`
	Content string `json:"content"`
	AvatarURL string `json:"avatar_url"`
	Tts bool `json:"tts"`
}

func WebHookEntry(what string, URL string) bool {
	if strings.Contains(URL, ".com") {
		info := WebhookInfo{}
		info.Username = "Roberto"
		info.AvatarURL = "https://i.kinja-img.com/gawker-media/image/upload/s--X_g6diE9--" +
			"/c_scale,fl_progressive,q_80,w_800/u7f2np5cy6pxuv7zijec.jpg"
		info.Tts = true
		info.Content = what + "\n"
		raw, _ := json.Marshal(info)
		resp, err := http.Post(URL, "application/json", bytes.NewBuffer(raw))
		if err != nil {
			log.Println(err)
			log.Println(ioutil.ReadAll(resp.Body))
			return false
		}
		return true
	} else {
		return false
	}

}

func webhookMain() {

	//println("Heroku timer test at: " + time.Now().String())
	//delay := time.Second

	db := GetADB()
	db.Init()
	db.DailyCheckHooks()

	DailyRoutine()
	//time.Sleep(delay)

}
